const Joi = require('joi');

// truck validation
const truckValidation = Joi.object({
  _id: Joi.any(),

  created_by: Joi.string().required(),

  assigned_to: Joi.string().required(),

  status: Joi.string().valid('IS', 'OL').required(),

  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),

  created_date: Joi.date().required(),
});

module.exports = truckValidation;
