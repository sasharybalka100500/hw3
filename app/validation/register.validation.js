const Joi = require('joi');

// user register validation
const registerValidation = Joi.object({
  _id: Joi.any(),

  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: {allow: ['com', 'net']},
  }),

  password: Joi.string().min(5).required(),

  role: Joi.string().valid('DRIVER', 'SHIPPER').required(),

  resetPasswordToken: Joi.string(),

  resetPasswordExpires: Joi.any(),
});

module.exports = registerValidation;
