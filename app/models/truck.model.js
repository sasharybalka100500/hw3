const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema(
    {
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
      },
      type: {
        type: String,
      },
      status: {
        type: String,
      },
      created_date: {
        type: Date,
      },
    },
    {
      collection: 'trucks',
    },
);

module.exports = mongoose.model('Truck', truckSchema);
