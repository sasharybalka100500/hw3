const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
      role: {
        type: String,
      },
      email: {
        type: String,
        unique: true,
        required: 'Email is required',
      },
      password: {
        type: String,
        min: [5, 'Too short, min is 5 characters'],
        max: [32, 'Too long, max is 15 characters'],
        required: 'Password is required',
      },

      created_date: {type: Date},
    },
    {
      collection: 'users',
    },
);

module.exports = mongoose.model('user', userSchema);
