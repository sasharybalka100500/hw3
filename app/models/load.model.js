const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema(
    {
      created_by: {
        type: String,
      },

      assigned_to: {
        type: String,
      },
      status: {
        type: String,
      },
      state: {
        type: String,
      },
      name: {
        type: String,
      },
      payload: {
        type: Number,
      },
      pickup_address: {type: String},
      delivery_address: {type: String},
      dimensions: {
        type: Object,
        of: Number,
      },

      logs: {
        type: Map,
        of: String,
      },
      created_date: {
        type: Date,
      },
    },
    {
      collection: 'loads',
    },
);

module.exports = mongoose.model('load', loadSchema);
