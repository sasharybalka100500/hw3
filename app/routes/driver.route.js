module.exports = (app) => {
  require('dotenv').config();

  const withAuth = require('../../configs/auth');
  truckValidation = require('../validation/truck.validation');
  User = require('../models/user.model');
  const mongoose = require('mongoose');

  // Models
  const TruckSchema = require('../models/truck.model');
  const loadSchema = require('../models/load.model');

  // CREATE truck
  app.post('/api/trucks', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can create truck');
      return res.status(400).json({message: 'Only drivers can create truck'});
    }
    const truck = new TruckSchema({
      created_by: req.user.user_id,
      assigned_to: 'nobody',
      type: req.body.type.toUpperCase(),
      status: 'IS',
      created_date: Date.now(),
    });

    try {
      await truckValidation.validateAsync(truck._doc);
      await truck.save();
      console.log('Truck created successfully');
      return res.status(200).json({
        message: 'Truck created successfully',
      });
    } catch (err) {
      console.log('Server error');
      return res.status(500).json({message: 'Server error'});
    }
  });

  // READ all users trucks
  app.get('/api/trucks', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can watch trucks');
      return res.status(400).json({message: 'Only drivers can watch trucks'});
    }
    TruckSchema.find({created_by: req.user.user_id}, (error, data) => {
      if (error) {
        return next(error);
      } else {
        console.log('Success');
        return res.status(200).json({
          trucks: data,
        });
      }
    });
  });

  // get truck by id
  app.get('/api/trucks/:id', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can watch trucks');
      return res.status(400).json({message: 'Only drivers can watch truck'});
    }
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      await TruckSchema.findById(req.params.id).then((targetTruck) => {
        console.log('Success');
        return res.status(200).json({
          truck: targetTruck,
        });
      });
    } else {
      console.log(`Truck with id  ${req.params.id} doesn't exist`);
      return res.status(400).json({
        message: `Truck with id  ${req.params.id} doesn't exist`,
      });
    }
  });

  // assign truck to current driver
  app.post('/api/trucks/:id/assign', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can assign truck');
      return res.status(400).json({message: 'Only drivers can assign truck'});
    }
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      TruckSchema.find({assigned_to: req.user.user_id}, (error, data) => {
        if (data.length > 0) {
          console.log(`Driver can assign only one truck`);
          return res.status(400).json({
            message: `Driver can assign only one truck`,
          });
        } else {
          TruckSchema.findById(req.params.id).then((targetTruck) => {
            if (targetTruck.assigned_to !== 'nobody') {
              console.log(`Truck with id  ${req.params.id} already assigned`);
              return res.status(400).json({
                message: `Truck with id  ${req.params.id} already assigned`,
              });
            }

            targetTruck.assigned_to = req.user.user_id;
            targetTruck.save();
            console.log('Truck assigned successfully');
            return res.status(200).json({
              message: 'Truck assigned successfully',
            });
          });
        }
      });
    } else {
      console.log(`Truck with id  ${req.params.id} doesn't exist`);
      return res.status(400).json({
        message: `Truck with id  ${req.params.id} doesn't exist`,
      });
    }
  });

  // UPDATE not assigned to driver trucks info
  app.put('/api/trucks/:id', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can edit truck');
      return res.status(400).json({message: 'Only drivers can edit truck'});
    }
    TruckSchema.findById(req.params.id, async (err, TruckSchema) => {
      if (!TruckSchema) {
        console.log('Truck is not found');
        return res.status(404).json({message: 'Truck is not found'});
      }

      try {
        if (!req.body.type) {
          console.log('Invalid credentials');
          return res.status(404).json({message: 'Invalid credentials'});
        }
        if (TruckSchema.assigned_to === 'nobody') {
          TruckSchema.type = req.body.type;
        } else {
          console.log(
              'This truck is assigned. You cant change info of assigned truck',
          );
          return res.status(404).json({
            message:
              'This truck is assigned. You cant change info of assigned truck',
          });
        }

        TruckSchema.save();
        return res
            .status(200)
            .json({message: 'Truck details changed successfully'});
      } catch (err) {
        console.log('Server error');
        return res.status(500).json({message: 'Server error'});
      }
    });
  });

  // DELETE not assigned to driver trucks
  app.delete('/api/trucks/:id', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can delete truck');
      return res.status(400).json({message: 'Only drivers can delete truck'});
    }
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      await TruckSchema.findById(req.params.id).then((targetTruck) => {
        if (!targetTruck) {
          console.log('Invalid id');
          return res.status(400).json({
            message: 'Invalid id',
          });
        }
        if (targetTruck.assigned_to !== 'nobody') {
          console.log(
              'This truck is assigned. You can not delete assigned truck.',
          );
          return res.status(400).json({
            message:
              'This truck is assigned. You can not delete assigned truck.',
          });
        } else {
          targetTruck.remove();
          console.log('Truck deleted successfully');
          return res.status(200).json({
            message: 'Truck deleted successfully',
          });
        }
      });
    } else {
      console.log(`Truck with id  ${req.params.id} doesn't exist`);
      return res.status(400).json({
        message: `Truck with id  ${req.params.id} doesn't exist`,
      });
    }
  });

  // get driver active load if exist
  app.get('/api/loads/active', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can do it');
      return res.status(400).json({message: 'Only drivers can do it'});
    } else {
      TruckSchema.findOne({assigned_to: req.user.user_id}, (error, truck) => {
        if (!truck) {
          return res
              .status(400)
              .json({message: 'Driver not have assigned trucks'});
        }
        loadSchema.findOne(
            {assigned_to: truck._id, status: 'ASSIGNED'},
            (error, data) => {
              if (!data) {
                return res
                    .status(400)
                    .json({message: 'Driver not have active loads'});
              } else {
                console.log('Success');
                return res.status(200).json({
                  load: data,
                });
              }
            },
        );
      });
    }
  });

  // change state active load
  app.patch('/api/loads/active/state', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'DRIVER') {
      console.log('Only drivers can do it');
      return res.status(400).json({message: 'Only drivers can do it'});
    } else {
      TruckSchema.findOne({assigned_to: req.user.user_id}, (error, truck) => {
        if (!truck) {
          return res
              .status(400)
              .json({message: 'Driver not have assigned trucks'});
        }
        loadSchema.findOne(
            {assigned_to: truck._id, status: 'ASSIGNED'},
            async (error, data) => {
              if (!data) {
                return res
                    .status(400)
                    .json({message: 'Driver not have active loads'});
              } else {
                if (data.state === 'En route to Pick Up') {
                  data.state = 'Arrived to Pick Up';
                } else if (data.state === 'Arrived to Pick Up') {
                  data.state = 'En route to Delivery';
                } else if (data.state === 'En route to Delivery') {
                  data.state = 'Arrived to Delivery';
                }
                if (data.state === 'Arrived to Delivery') {
                  try {
                    data.status = 'SHIPPED';
                    truck.status = 'IS';
                    truck.save();
                    data.save();
                    console.log('Success');
                    // return res.status(200).send({load: data, truck});
                    return res.status(200).json({
                      message: `Load state changed to ${data.state}`,
                    });
                  } catch (err) {
                    console.log('Server error');
                    return res.status(500).send({message: 'Server error'});
                  }
                }
                await data.save();
                console.log(`Load state changed to ${data.state}`);
                return res.status(200).json({
                  message: `Load state changed to ${data.state}`,
                });
              }
            },
        );
      });
    }
  });
};
