module.exports = (app) => {
  require('dotenv').config();

  const withAuth = require('../../configs/auth');
  loadValidation = require('../validation/load.validation');
  mongoose = require('mongoose');

  // load Model
  const LoadSchema = require('../models/load.model');
  const truckSchema = require('../models/truck.model');

  // CREATE load
  app.post('/api/loads', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      return res
          .status(400)
          .json({message: 'Only shippers can create loads'});
    }
    // if (numberOfLoads.length < 5) {
    const load = new LoadSchema({
      created_by: req.user.user_id,
      assigned_to: 'nobody',
      status: 'NEW',
      state: '',
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: req.body.dimensions,
      logs: [],
      created_date: Date.now(),
    });

    await loadValidation.validateAsync(load._doc);
    await load.save();
    try {
      console.log('Load created successfully');
      return res.status(200).json({
        message: 'Load created successfully',
      });
    } catch (err) {
      console.log('Something wrong');
      return res.status(400).json({message: 'Something wrong'});
    }
  });

  // READ all loads
  app.get('/api/loads', withAuth, async (req, res) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      truckSchema.findOne({assigned_to: req.user.user_id}, (error, truck) => {
        if (!truck) {
          return res
              .status(400)
              .json({message: 'Not found assigned to current driver truck'});
        }
        LoadSchema.find({assigned_to: truck._id}, (error, load) => {
          if (!load) {
            console.log('Not found assigned to current driver loads');
            return res.status(400).json({
              message: 'Not found assigned to current driver loads',
            });
          } else {
            console.log('Success');
            return res.status(200).json({loads: load});
          }
        });
      });
    } else {
      LoadSchema.find({created_by: req.user.user_id}, (error, data) => {
        if (error) {
          return next(error);
        } else {
          console.log('Success');
          return res.status(200).json({
            loads: data,
          });
        }
      });
    }
  });

  // UPDATE shipper load with status "NEW"
  app.put('/api/loads/:id', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      return res
          .status(400)
          .json({message: 'Only shippers can update loads'});
    }
    LoadSchema.findById(req.params.id, (err, LoadSchema) => {
      console.log(LoadSchema);
      if (!LoadSchema) {
        console.log('load is not found');
        return res.status(404).json({message: 'load is not found'});
      }

      if (LoadSchema.status === 'NEW') {
        LoadSchema.name = req.body.name || LoadSchema.name;
        LoadSchema.pickup_address =
          req.body.pickup_address || LoadSchema.pickup_address;
        LoadSchema.delivery_address =
          req.body.delivery_address || LoadSchema.delivery_address;
        LoadSchema.dimensions = req.body.dimensions || LoadSchema.dimensions;
        LoadSchema.payload = req.body.payload || LoadSchema.payload;
      } else {
        return res
            .status(400)
            .json({message: 'Only loads with status "NEW" can be modified.'});
      }

      try {
        LoadSchema.save();
        return res
            .status(200)
            .json({message: 'Load details changed successfully'});
      } catch (err) {
        console.log('Server error');
        return res.status(500).json({message: 'Server error'});
      }
    });
  });

  // DELETE not assigned to driver loads
  app.delete('/api/loads/:id', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      return res
          .status(400)
          .json({message: 'Only shippers can delete loads'});
    }
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      await LoadSchema.findById(req.params.id).then((targetLoad) => {
        if (!targetLoad) {
          console.log('Invalid id');
          return res.status(400).json({
            message: 'Invalid id',
          });
        }
        if (targetLoad.status !== 'NEW') {
          console.log('You can delete only loads with status NEW.');
          return res.status(400).json({
            message: 'You can delete only loads with status NEW.',
          });
        } else {
          targetLoad.remove();
          console.log('Load deleted successfully');
          return res.status(200).json({
            message: 'Load deleted successfully',
          });
        }
      });
    } else {
      console.log(`Truck with id  ${req.params.id} doesn't exist`);
      return res.status(400).json({
        message: `Truck with id  ${req.params.id} doesn't exist`,
      });
    }
  });

  // READ single shipper loads
  app.get('/api/loads/:id', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      console.log('Only shippers can do it');
      return res.status(400).json({message: 'Only shippers can do it'});
    }
    LoadSchema.findOne(
        {_id: req.params.id, created_by: req.user.user_id},
        (error, data) => {
          if (!data) {
            return res
                .status(400)
                .json({message: 'Current user dont have load with current id'});
          }
          if (error) {
            console.log('Error');
            return res.status(400).json({message: 'Error'});
          } else {
            console.log('Success');
            return res.status(200).json({load: data});
          }
        },
    );
  });

  // POST a load
  app.post('/api/loads/:id/post', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      console.log('Only shippers can do it');
      return res.status(400).json({message: 'Only shippers can do it'});
    }
    try {
      const load = await LoadSchema.findById({_id: req.params.id});

      const trucks = await truckSchema.find({status: 'IS'});
      const truck = trucks.find((el) => el.assigned_to !== 'nobody');
      console.log(truck);
      if (!load) {
        console.log('Not find load with this id');
        return res.status(400).json({message: 'Not find load with this id'});
      }
      if (load.status !== 'NEW') {
        return res
            .status(400)
            .json({message: 'You can post only loads with status NEW'});
      }

      if (!truck) {
        load.logs = {
          message: 'There is no truck in service.',
          time: new Date(),
        };
        load.status = 'NEW';
        load.save();
        return res
            .status(400)
            .json({message: 'There is no truck in service.'});
      } else {
        const loadPayload = Object.entries({load: load.payload})[0][1];

        const sizes = Object.entries({load: load.dimensions})[0][1];
        const loadWidth = Object.entries(sizes)[0][1];
        const loadLength = Object.entries(sizes)[1][1];
        const loadHeight = Object.entries(sizes)[2][1];

        const truckType = Object.entries({truck: truck.type})[0][1];
        if (
          loadWidth > 200 ||
          loadHeight > 350 ||
          loadLength > 700 ||
          loadPayload > 4000
        ) {
          return res.status(400).json({message: 'Sorry, load is very large'});
        }
        if (truckType === 'SPRINTER') {
          const truckLength = 300;
          const truckHeight = 250;
          const truckWidth = 170;
          const truckPayload = 1700;

          if (
            loadWidth < truckWidth &&
            loadLength < truckLength &&
            loadHeight < truckHeight &&
            loadPayload < truckPayload
          ) {
            try {
              load.status = 'ASSIGNED';
              load.state = 'En route to Pick Up';
              load.assigned_to = truck._id;
              load.logs = {
                message: `Assigned to truck with id: ${truck._id}`,
                time: new Date(),
              };
              truck.status = 'OL';

              truck.save();
              load.save();
              console.log('Load posted successfully');
              return res.status(200).json({
                message: 'Load posted successfully',
                driver_found: true,
              });
            } catch (err) {
              console.log('Server error');
              return res.status(500).json({message: 'Server error'});
            }
          }
        } else if (truckType === 'SMALL STRAIGHT') {
          const truckLength = 500;
          const truckHeight = 250;
          const truckWidth = 170;
          const truckPayload = 2500;

          if (
            loadWidth < truckWidth &&
            loadLength < truckLength &&
            loadHeight < truckHeight &&
            loadPayload < truckPayload
          ) {
            try {
              load.status = 'ASSIGNED';
              load.state = 'En route to Pick Up';
              load.assigned_to = truck._id;
              load.logs = {
                message: `Assigned to truck with id: ${truck._id}`,
                time: new Date(),
              };
              truck.status = 'OL';
              truck.save();
              load.save();
              console.log('Load posted successfully');
              return res.status(200).json({
                message: 'Load posted successfully',
                driver_found: true,
              });
            } catch (err) {
              console.log('Server error');
              return res.status(500).json({message: 'Server error'});
            }
          }
        } else if (truckType === 'LARGE STRAIGHT') {
          const truckLength = 700;
          const truckHeight = 350;
          const truckWidth = 200;
          const truckPayload = 4000;

          if (
            loadWidth < truckWidth &&
            loadLength < truckLength &&
            loadHeight < truckHeight &&
            loadPayload < truckPayload
          ) {
            try {
              load.status = 'ASSIGNED';
              load.state = 'En route to Pick Up';
              load.assigned_to = truck._id;
              load.logs = {
                message: `Assigned to truck with id: ${truck._id}`,
                time: new Date(),
              };
              truck.status = 'OL';
              truck.save();
              load.save();
            } catch (err) {
              console.log('Server error');
              return res.status(500).json({message: 'Server error'});
            }
          }
        }
      }
    } catch (err) {
      console.log('Server error');
      return res.status(500).json({message: 'Server error'});
    }
  });

  // View shipping info
  app.get('/api/loads/:id/shipping_info', withAuth, async (req, res, next) => {
    const currentUser = await User.findById(req.user.user_id);
    if (currentUser.role !== 'SHIPPER') {
      console.log('Only shippers can do it');
      return res.status(400).json({message: 'Only shippers can do it'});
    }
    LoadSchema.findById(req.params.id, (error, data) => {
      if (error) {
        return next(error);
      } else {
        truckSchema.findById(data.assigned_to, (error, truck) => {
          console.log('Success');
          return res.status(200).json({
            load: data,
            truck: truck,
          });
        });
      }
    });
  });
};
