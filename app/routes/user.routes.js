module.exports = (app) => {
  const User = require('../models/user.model.js');
  const bcrypt = require('bcryptjs');
  const jwt = require('jsonwebtoken');
  const auth = require('../../configs/auth');

  app.post('/api/auth/register', async (req, res) => {
    try {
      const {email, password, role} = req.body;

      if (!(email && password && role)) {
        console.log('All input is required');
        return res.status(400).send({message: 'All input is required'});
      }
      if (role !== 'DRIVER' && role !== 'SHIPPER') {
        console.log('Invalid role');
        return res.status(400).send({message: 'Invalid role'});
      }
      const oldUser = await User.findOne({email});

      if (oldUser) {
        console.log('User Already Exist. Please Login');
        return res
            .status(400)
            .send({message: 'User Already Exist. Please Login'});
      }

      encryptedPassword = await bcrypt.hash(password, 10);

      await User.create({
        password: encryptedPassword,
        role,
        email,
        created_date: Date.now(),
      });

      console.log('Profile created successfully');
      res.status(200).send({message: 'Profile created successfully'});
    } catch (err) {
      console.log('Server error');
      res.status(500).send({message: 'Server error'});
    }
  });
  app.post('/api/auth/login', async (req, res) => {
    try {
      const {email, password} = req.body;

      if (!(email && password)) {
        console.log('All inputs is required');
        return res.status(400).send({message: 'All inputs is required'});
      }
      const user = await User.findOne({email});

      if (user && (await bcrypt.compare(password, user.password))) {
        const token = jwt.sign(
            {
              user_id: user._id,
              password: user.password,
            },
            process.env.TOKEN_KEY,
            {
              expiresIn: '24h',
            },
        );

        user.token = token;
        console.log('Success');
        return res.status(200).send({jwt_token: user.token});
      }
      console.log('Invalid Credentials');
      return res.status(400).send({message: 'Invalid Credentials'});
    } catch (err) {
      console.log('Server error');
      res.status(500).send({message: 'Server error'});
    }
  });
  app.get('/api/users/me', auth, async (req, res) => {
    try {
      const user = await User.findById(req.user.user_id).select('-password');
      console.log('Success');
      res.status(200).send({user});
    } catch (err) {
      console.log(`Server error`);
      res.status(500).send({message: `Server error`});
    }
  });
  app.delete('/api/users/me', auth, async (req, res) => {
    try {
      User.findByIdAndRemove(req.user.user_id, (err, docs) => {
        if (err) {
          console.log('Something wrong');
          res.status(400).send({message: `Something wrong`});
        } else {
          console.log('Profile deleted successfully');
          res.status(200).send({message: `Profile deleted successfully`});
        }
      });
    } catch (err) {
      console.log(`Server error`);
      res.status(500).send({message: `Server error`});
    }
  });
  app.patch('/api/users/me/password', auth, async (req, res) => {
    try {
      if (!(req.body.oldPassword && req.body.newPassword)) {
        console.log(`Invalid Credentials`);
        return res.status(400).send({message: `Invalid Credentials`});
      }
      if (await bcrypt.compare(req.body.oldPassword, req.user.password)) {
        const pass = await bcrypt.hash(req.body.newPassword, 10);
        User.findByIdAndUpdate(
            {
              _id: req.user.user_id,
            },
            {
              $set: {password: pass},
            },
        ).then(() => {
          console.log(`Password changed successfully`);
          res.status(200).send({message: `Password changed successfully`});
        });
      } else {
        console.log(`Invalid Password`);
        res.status(400).send({message: `Invalid Password`});
      }
    } catch (err) {
      console.log(`Server error`);
      res.status(500).send({message: `Server error`});
    }
  });
};
