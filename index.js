require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());
const mongoose = require('mongoose');
const PORT = process.env.PORT;
mongoose.Promise = global.Promise;

require('./app/routes/user.routes.js')(app);
require('./app/routes/driver.route.js')(app);
require('./app/routes/shipper.route.js')(app);
const start = async () => {
  try {
    await mongoose.connect(
        'mongodb+srv://qwerty:qwerty123@cluster0.7fiyq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    );
    app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
  } catch (error) {
    console.log(error);
  }
};
start();
